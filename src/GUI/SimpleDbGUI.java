package GUI;

import database.insert.Student;
import database.insert.Tutor;
import database.report.StudentReport;
import database.report.TutorReport;

import java.sql.Date;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class SimpleDbGUI {

    private static final Logger log = Logger.getLogger(SimpleDbGUI.class.getName());

    public static void main(String[] args) {

        int choice = 0;

        do {
            System.out.println("SSC EX2");
            System.out.println("University DataBase");

            System.out.println("SID: 1367219");

            System.out.println("\n\nPlease Select an option - ");
            System.out.println("\t1. Add a new student." +
                    "\n\t2. Add a new tutor for a student." +
                    "\n\t3. Generate a report for a Student." +
                    "\n\t4. Generate a report for a Tutor.");

            Scanner in = new Scanner(System.in);
            choice = in.nextInt();
            in.nextLine();

            switch (choice) {

                case 1:
                    addStudent();
                    break;
                case 2:
                    addTutor();
                    break;
                case 3:
                    stuReport();
                    break;
                case 4:
                    tutReport();
                    break;
                default:
                    System.out.println("Please input a proper value");
                    break;
            }
        } while (choice <= 0 || choice >= 5);
    }

    public static void addStudent() {

        Scanner input = new Scanner(System.in);

        System.out.println("Enter data for new student - \n");

        System.out.print("Student ID: ");
        int sid = input.nextInt();
        input.nextLine();

        System.out.print("Title: ");
        String title = input.nextLine();

        System.out.print("First Name: ");
        String fname = input.nextLine();

        System.out.print("Last Name: ");
        String lname = input.nextLine();

        System.out.println("Date of Birth: \n ");
        System.out.print("Year (yyyy): ");
        int year = input.nextInt();
        input.nextLine();

        System.out.print("Month (mm): ");
        int month = input.nextInt();
        input.nextLine();

        System.out.print("Day (dd): ");
        int day = input.nextInt();
        input.nextLine();

        Student stu = new Student(sid,
                title,
                fname,
                lname,
                new Date(year - 1900, month - 1, day));
        stu.insert();
    }

    public static void addTutor() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter data for New Tutor - \n");

        System.out.print("Student ID: ");
        int sid = input.nextInt();
        input.nextLine();

        System.out.print("Tutor ID: ");
        String lid = input.nextLine();

        Tutor tut = new Tutor(sid, lid);
        int result = tut.update();

        if (result == 0) {
            tut.insert();
        }
    }

    public static void stuReport() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter data to generate report for Student - \n");

        System.out.print("Student ID: ");
        int sid = input.nextInt();
        input.nextLine();

        StudentReport studentReport = new StudentReport(sid);
        studentReport.generateReport();
    }

    public static void tutReport() {

        Scanner input = new Scanner(System.in);
        System.out.println("Enter data to generate report for Tutor - \n");

        System.out.print("Tutor ID: ");
        String lid = input.nextLine();

        TutorReport tutorReport = new TutorReport(lid);
        tutorReport.generateReport();
    }
}
