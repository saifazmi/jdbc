package database.properties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : UniDB
 * @date : 22/10/15
 */
public class DBProperty {

    private static final Logger log = Logger.getLogger(DBProperty.class.getName());
    private final String propertyFileName = "database.properties";

    //TODO: refine fields to fit the proper descriptions of a db property file.
    private String drivers;
    private String serverURL;
    private String dbName;
    private String username;
    private String password;

    public DBProperty() {

        readPropertyFile();
    }

    public DBProperty(String dbName, String username, String password) {

        this.drivers = "org.postgresql.Driver";
        this.serverURL = "jdbc:postgresql://dbteach2.cs.bham.ac.uk/";
        this.dbName = dbName;
        this.username = username;
        this.password = password;

        createPropertyFile();
    }

    public DBProperty(String serverURL, String dbName, String username, String password) {

        this.drivers = "org.postgresql.Driver";
        this.serverURL = serverURL;
        this.dbName = dbName;
        this.username = username;
        this.password = password;

        createPropertyFile();
    }

    private void createPropertyFile() {

        FileOutputStream fileOut = null;

        try {
            Properties dbProperties = new Properties();
            dbProperties.setProperty("db.drivers", getDrivers());
            dbProperties.setProperty("db.serverURL", getServerURL());
            dbProperties.setProperty("db.name", getDbName());
            dbProperties.setProperty("db.username", getUsername());
            dbProperties.setProperty("db.password", getPassword());

            File dbPropertyFile = new File("./resources/" + getPropertyFileName());
            fileOut = new FileOutputStream(dbPropertyFile);
            dbProperties.store(fileOut, "Database login credentials");
        } catch (FileNotFoundException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } finally {
            try {
                if (fileOut != null)
                    fileOut.close();
            } catch (IOException e) {
                log.log(Level.SEVERE, e.toString(), e);
                e.printStackTrace();
            }
        }
    }

    private void readPropertyFile() {

        InputStream inputStream = null;

        try {

            inputStream = new FileInputStream("./resources/" + getPropertyFileName());

            Properties dbProperties = new Properties();
            dbProperties.load(inputStream);

            this.drivers = dbProperties.getProperty("db.drivers");
            this.serverURL = dbProperties.getProperty("db.serverURL");
            this.dbName = dbProperties.getProperty("db.name");
            this.username = dbProperties.getProperty("db.username");
            this.password = dbProperties.getProperty("db.password");
        } catch (IOException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        } finally {
            try {
                if (inputStream != null)
                    inputStream.close();
            } catch (IOException e) {
                log.log(Level.SEVERE, e.toString(), e);
                e.printStackTrace();
            }
        }
    }


    private String getPropertyFileName() {
        return propertyFileName;
    }

    public String getDrivers() {
        return drivers;
    }

    public String getServerURL() {
        return serverURL;
    }

    public String getDbName() {
        return dbName;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
