package database;

import database.insert.Lecturer;
import database.insert.LecturerContact;
import database.insert.RegistrationType;
import database.insert.Student;
import database.insert.StudentContact;
import database.insert.StudentKinContact;
import database.insert.StudentRegistration;
import database.insert.Titles;
import database.insert.Tutor;

import java.sql.Date;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class Populate {

    private static final Logger log = Logger.getLogger(Populate.class.getName());

    public static void main(String[] args) {

        populateTitles();
        populateRegType();
        populateLecturer();
        populateLecturerContact();
        populateStudent();
        populateStudentContact();
        populateTutor();
        populateStuKinContact();
        populateStuRegistration();
    }

    /**
     * Populates the Student_Registration table
     */
    public static void populateStuRegistration() {
        int numberOfRowsInserted = 0;

        StudentRegistration stuReg;
        /**
         * Fake Data
         */
        int baseID = 1367228;
        String[] regType = {"NR", "EX", "RP", "TF"};
        Random rand = new Random();

        /**
         * Real Data
         */
        // Stu 1
        stuReg = new StudentRegistration(1367219, 2, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 2
        stuReg = new StudentRegistration(1367220, 2, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 3
        stuReg = new StudentRegistration(1367221, 1, "TF");
        numberOfRowsInserted += stuReg.insert();

        // Stu 4
        stuReg = new StudentRegistration(1367222, 1, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 5
        stuReg = new StudentRegistration(1367223, 3, "EX");
        numberOfRowsInserted += stuReg.insert();

        // Stu 6
        stuReg = new StudentRegistration(1367224, 3, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 7
        stuReg = new StudentRegistration(1367225, 4, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 8
        stuReg = new StudentRegistration(1367226, 4, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 9
        stuReg = new StudentRegistration(1367227, 5, "NR");
        numberOfRowsInserted += stuReg.insert();

        // Stu 10
        stuReg = new StudentRegistration(1367228, 5, "RP");
        numberOfRowsInserted += stuReg.insert();

        for (int i = 11; i <= 100; i++) {

            baseID++;
            int randomYOS = rand.nextInt((5 - 1) + 1) + 1;
            int randomREG = rand.nextInt(regType.length);

            stuReg = new StudentRegistration(baseID, randomYOS, regType[randomREG]);
            numberOfRowsInserted += stuReg.insert();
        }

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the Student_Kin_Contact table
     */
    public static void populateStuKinContact() {
        int numberOfRowsInserted = 0;

        StudentKinContact stuKin;

        /**
         * Fake Data
         */
        int baseID = 1367228;
        String baseKinName = "\'s Kin";
        String baseEmail = "@nextkin.com";
        int baseHNo = 28;

        /**
         * Real Data
         */
        // Stu 1
        stuKin = new StudentKinContact(1367219, "Shabana Azmi", "s.azmi@ymail.com", "88 SKV, K Nagar, India");
        numberOfRowsInserted += stuKin.insert();

        // Stu2
        stuKin = new StudentKinContact(1367220, "Carl Doe", "c.doe@domain.com", "House 19, D Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu3
        stuKin = new StudentKinContact(1367221, "Jared Cobbin", "j.r.cobbin@mail.com", "House 26, L Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu4
        stuKin = new StudentKinContact(1367222, "Mushtak Rahim", "m.k.rahim@domain.com", "House 20, A Road, Cambridge, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu5
        stuKin = new StudentKinContact(1367223, "Lucy Jones", "jones.l@gigamail.com", "House 28, F Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu6
        stuKin = new StudentKinContact(1367224, "Matthew Aldwin", "m.ald@domain.com", "House 26, L Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu7
        stuKin = new StudentKinContact(1367225, "Paresh Rao", "p.l.rao@gmail.com", "House 23, N Road, Exter, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu8
        stuKin = new StudentKinContact(1367226, "Hammad Azmi", "hmm.az@domain.com", "House 27, BL Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu9
        stuKin = new StudentKinContact(1367227, "Chun Lee", "c.lee@lee.ninja", "House 29, BK Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        // Stu10
        stuKin = new StudentKinContact(1367228, "Foo Bar Sr.", "big.bar@foo.com", "House 31, BL Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuKin.insert();

        for (int i = 11; i <= 100; i++) {
            baseID++;
            baseKinName = baseID+baseKinName;
            baseEmail = baseID + baseEmail;
            baseHNo++;
            String baseAdd = "House " + baseHNo + ", X Rd, Bham, UK, B15 2TT";

            stuKin = new StudentKinContact(baseID, baseKinName, baseEmail, baseAdd);
            numberOfRowsInserted += stuKin.insert();

            baseKinName = "\'s Kin";
            baseEmail = "@nextkin.com";

        }

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the Tutor table
     */
    public static void populateTutor() {
        int numberOfRowsInserted = 0;

        Tutor tut;

        /**
         * Fake Data
         */
        int baseID = 1367228;
        String[] tutors = {"rjh1206", "szh1104", "exr1304", "mgl0910", "rzb1107"};
        Random rand = new Random();


        /**
         * Real Data
         */
        // Stu1
        tut = new Tutor(1367219, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu2
        tut = new Tutor(1367220, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu3
        tut = new Tutor(1367221, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu4
        tut = new Tutor(1367222, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu5
        tut = new Tutor(1367223, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu6
        tut = new Tutor(1367224, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu7
        tut = new Tutor(1367225, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu8
        tut = new Tutor(1367226, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu9
        tut = new Tutor(1367227, "exr1304");
        numberOfRowsInserted += tut.insert();

        // Stu10
        tut = new Tutor(1367228, "exr1304");
        numberOfRowsInserted += tut.insert();

        for (int i = 11; i <= 100; i++) {
            baseID++;
            int randomTut = rand.nextInt(tutors.length);
            tut = new Tutor(baseID, tutors[randomTut]);
            numberOfRowsInserted += tut.insert();
        }

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the Student_Contact table
     */
    public static void populateStudentContact() {
        int numberOfRowsInserted = 0;

        StudentContact stuContact;

        /**
         * Fake data
         */
        int baseID = 1367228;
        String baseEmail = "stu@domain.com";
        int baseHNo = 28;

        /**
         * Some real data
         */

        // Stu 1
        stuContact = new StudentContact(1367219, "me@saifazmi.com", "House 19, D Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 2
        stuContact = new StudentContact(1367220, "john@jdomain.com", "House 20, A Road, Cambridge, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 3
        stuContact = new StudentContact(1367221, "mad@cobbin.com", "House 21, C Road, Manchester, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 4
        stuContact = new StudentContact(1367222, "rahim.adil@domain.com", "House 22, E Road, London, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 5
        stuContact = new StudentContact(1367223, "mc.hana@domain.com", "House 23, N Road, Exter, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 6
        stuContact = new StudentContact(1367224, "aldi.win@ralph.com", "House 24, G Road, Manchester, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 7
        stuContact = new StudentContact(1367225, "a.rao@domain.com", "House 25, H Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 8
        stuContact = new StudentContact(1367226, "k.a@azmi.com", "House 26, L Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 9
        stuContact = new StudentContact(1367227, "be.lee.or.be.me@bruce.ninja", "House 27, BL Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        // Stu 10
        stuContact = new StudentContact(1367228, "foo@bar.com", "House 28, F Road, Birmingham, UK, B15 2TT");
        numberOfRowsInserted += stuContact.insert();

        for (int i = 11; i <= 100; i++) {
            baseID++;
            baseEmail = baseID + baseEmail;
            baseHNo++;
            String baseAdd = "House " + baseHNo + ", X Rd, Bham, UK, B15 2TT";

            stuContact = new StudentContact(baseID, baseEmail, baseAdd);
            numberOfRowsInserted += stuContact.insert();

            baseEmail = "stu@domain.com";
        }

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the Student table
     */
    public static void populateStudent() {
        int numberOfRowsInserted = 0;

        Student stu;

        /**
         * Dummy data
         */
        int baseID = 1367228;
        String baseTitle = "Mx";
        String baseForeName = "ForeName";
        String baseLastName = "LastName";
        int baseDOBYear = 1990 - 1900;
        int baseDOBMonth = 0;
        int baseDOBDay = 1;


        /**
         * Some real data
         */

        // Stu 1
        stu = new Student(1367219, "Mr", "Saif", "Azmi", new Date(95, 1, 11));
        numberOfRowsInserted += stu.insert();

        // Stu 2
        stu = new Student(1367220, "Mr", "John", "Doe", new Date(96, 10, 11));
        numberOfRowsInserted += stu.insert();

        // Stu 3
        stu = new Student(1367221, "Ms", "Maddiline", "Cobbin", new Date(94, 3, 31));
        numberOfRowsInserted += stu.insert();

        // Stu 4
        stu = new Student(1367222, "Mr", "Adil", "Rahim", new Date(96, 5, 11));
        numberOfRowsInserted += stu.insert();

        // Stu 5
        stu = new Student(1367223, "Ms", "Hana", "McCarthy", new Date(94, 0, 4));
        numberOfRowsInserted += stu.insert();

        // Stu 6
        stu = new Student(1367224, "Mr", "Ralph", "Aldwin", new Date(94, 12, 21));
        numberOfRowsInserted += stu.insert();

        // Stu 7
        stu = new Student(1367225, "Ms", "Alka", "Rao", new Date(95, 8, 1));
        numberOfRowsInserted += stu.insert();

        // Stu 8
        stu = new Student(1367226, "Mr", "Khalid", "Azmi", new Date(90, 0, 1));
        numberOfRowsInserted += stu.insert();

        // Stu 9
        stu = new Student(1367227, "Mr", "Bruce", "Lee", new Date(40, 11, 27));
        numberOfRowsInserted += stu.insert();

        // Stu 10
        stu = new Student(1367228, "Mx", "Foo", "Bar", new Date((2012 - 1900), 12, 12));
        numberOfRowsInserted += stu.insert();

        for (int i = 11; i <= 100; i++) {
            baseID++;
            baseForeName += i;
            baseLastName += i;

            stu = new Student(baseID,
                    baseTitle,
                    baseForeName,
                    baseLastName,
                    new Date(baseDOBYear, baseDOBMonth, baseDOBDay));

            numberOfRowsInserted += stu.insert();

            if (i % 10 == 0) {
                baseDOBYear++;
            }
            if (baseDOBMonth < 11) {
                baseDOBMonth++;
            } else {
                baseDOBMonth = 1;
            }

            if (baseDOBDay < 28) {
                baseDOBDay++;
            } else {
                baseDOBDay = 1;
            }

            baseForeName = "ForeName";
            baseLastName = "LastName";
        }

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the Lecturer_Contact table
     */
    public static void populateLecturerContact() {
        int numberOfRowsInserted = 0;

        LecturerContact lecContact;

        // Bob
        lecContact = new LecturerContact("rjh1206", "203", "R.J.Hendley@cs.bham.ac.uk");
        numberOfRowsInserted += lecContact.insert();

        // Shan
        lecContact = new LecturerContact("szh1104", "UG36", "s.he@cs.bham.ac.uk");
        numberOfRowsInserted += lecContact.insert();

        // Eike
        lecContact = new LecturerContact("exr1304", "209", "E.Ritter@cs.bham.ac.uk");
        numberOfRowsInserted += lecContact.insert();

        // Mark
        lecContact = new LecturerContact("mgl0910", "123", "m.g.lee@cs.bham.ac.uk");
        numberOfRowsInserted += lecContact.insert();

        // Rami
        lecContact = new LecturerContact("rzb1107", "112", "r.bahsoon@cs.bham.ac.uk");
        numberOfRowsInserted += lecContact.insert();

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the Lecturer table
     */
    public static void populateLecturer() {
        int numberOfRowsInserted = 0;

        Lecturer lec;

        // Bob
        lec = new Lecturer("rjh1206", "Prof", "Robert", "Hendley");
        numberOfRowsInserted += lec.insert();

        // Shan
        lec = new Lecturer("szh1104", "Dr", "Shan", "He");
        numberOfRowsInserted += lec.insert();

        // Eike
        lec = new Lecturer("exr1304", "Dr", "Eike", "Ritter");
        numberOfRowsInserted += lec.insert();

        // Mark
        lec = new Lecturer("mgl0910", "Dr", "Mark", "Lee");
        numberOfRowsInserted += lec.insert();

        // Rami
        lec = new Lecturer("rzb1107", "Dr", "Rami", "Bahsoon");
        numberOfRowsInserted += lec.insert();

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }

    /**
     * Populates the RegistrationType table
     */
    public static void populateRegType() {
        int numberOfRowsInserted = 0;

        RegistrationType regType;

        // Normal
        regType = new RegistrationType("NR", "Normal");
        numberOfRowsInserted += regType.insert();

        // Repeat
        regType = new RegistrationType("RP", "Repeat");
        numberOfRowsInserted += regType.insert();

        // External
        regType = new RegistrationType("EX", "External");
        numberOfRowsInserted += regType.insert();

        // Transfer
        regType = new RegistrationType("TF", "Transfer");
        numberOfRowsInserted += regType.insert();

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }

    }

    /**
     * Populates the Title table
     */
    public static void populateTitles() {
        int numberOfRowsInserted = 0;

        Titles title;

        // Mister
        title = new Titles("Mr", "Mister");
        numberOfRowsInserted += title.insert();

        // Miss
        title = new Titles("Ms", "Miss");
        numberOfRowsInserted += title.insert();

        // Mx
        title = new Titles("Mx", "Do not wish to declare");
        numberOfRowsInserted += title.insert();

        // Professor
        title = new Titles("Prof", "Professor");
        numberOfRowsInserted += title.insert();

        // Doctor
        title = new Titles("Dr", "Doctor");
        numberOfRowsInserted += title.insert();

        if (numberOfRowsInserted != 0) {
            log.log(Level.INFO, "Successfully inserted " + numberOfRowsInserted + " row(s)");
        }
    }
}
