package database.report;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class StudentReport {

    private static final Logger log = Logger.getLogger(StudentReport.class.getName());

    private int sid;

    public StudentReport(int sid) {
        this.sid = sid;
    }

    public void generateReport() {

        String query = "SELECT S.studentid, S.forename, S.familyname, S.dob, T.description" +
                " FROM Student S, Titles T" +
                " WHERE studentid = ?" +
                " AND S.titleid = T.titleid;";

        ResultSet rs = null;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, sid);

            rs = prepStmt.executeQuery();

            String label = "Name";
            String line;
            while (rs.next()) {
                line = rs.getString("description") + ". " + rs.getString("forename") + " " + rs.getString("familyname");
                printReport(label, line);


                label = "Date of Birth";
                line = rs.getDate("dob").toString();
                printReport(label, line);

                label = "Student ID";
                line = rs.getInt("studentid") + "";
                printReport(label, line);
            }
            query = "SELECT SR.yos, RT.description" +
                    " FROM student_registration SR, registration_type RT" +
                    " WHERE SR.studentid = ?" +
                    " AND SR.regid = RT.regid;";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, sid);

            rs = prepStmt.executeQuery();
            while (rs.next()) {
                label = "Year of Study";
                line = rs.getInt("yos") + "";
                printReport(label, line);

                label = "Registration Type";
                line = rs.getString("description");
                printReport(label, line);
            }
            query = "SELECT email, postaladdress" +
                    " FROM student_contact" +
                    " WHERE studentid = ?;";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, sid);

            rs = prepStmt.executeQuery();
            while (rs.next()) {
                label = "Email";
                line = rs.getString("email");
                printReport(label, line);

                label = "Postal Address";
                line = rs.getString("postaladdress");
                printReport(label, line);
            }
            query = "SELECT name, email, postaladdress" +
                    " FROM student_kin_contact" +
                    " WHERE studentid = ?;";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, sid);

            rs = prepStmt.executeQuery();
            while (rs.next()) {
                label = "Emergency Contact";
                line = "\n" + rs.getString("name") +
                        "\n" + rs.getString("email") +
                        "\n" + rs.getString("postaladdress");
                printReport(label, line);
            }
            query = "SELECT L.forename, L.familyname" +
                    " FROM lecturer L, tutor T" +
                    " WHERE T.studentid = ?" +
                    " AND T.lecturerid = L.lecturerid;";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, sid);

            rs = prepStmt.executeQuery();
            while (rs.next()) {
                label = "Personal Tutor";
                line = rs.getString("forename") + " " + rs.getString("familyname");
                printReport(label, line);
            }
            if (rs != null) {
                log.log(Level.INFO, "Fetched data successfully");
            } else {
                log.log(Level.SEVERE, "No student exists with this id");
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }

    private void printReport(String label, String line) {

        System.out.print(label + ": ");
        System.out.println(line);
    }
}
