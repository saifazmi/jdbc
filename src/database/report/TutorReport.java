package database.report;

import database.insert.Tutor;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class TutorReport {

    private static final Logger log = Logger.getLogger(TutorReport.class.getName());

    private String lid;

    public TutorReport(String lid) {
        this.lid = lid;
    }

    public void generateReport() {

        String query = "SELECT titleid, forename, familyname" +
                " FROM lecturer" +
                " WHERE lecturerid = ?;";

        ResultSet rs = null;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lid);
            rs = prepStmt.executeQuery();
            while (rs.next()) {
                String label = "Name";
                String line = rs.getString("titleid") + ". " + rs.getString("forename") + " " + rs.getString("familyname");
                printReport(label, line);
            }
            printReport("\nYEAR 1", "\n");

            query = "SELECT studentid" +
                    " FROM tutor" +
                    " WHERE lecturerid = ?" +
                    " AND studentid IN (" +
                    " SELECT studentid" +
                    " FROM student_registration" +
                    " WHERE yos = 1);";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lid);
            rs = prepStmt.executeQuery();

            while (rs.next()) {
                StudentReport studentReport = new StudentReport(rs.getInt("studentid"));
                studentReport.generateReport();
            }

            printReport("\nYEAR 2", "\n");

            query = "SELECT studentid" +
                    " FROM tutor" +
                    " WHERE lecturerid = ?" +
                    " AND studentid IN (" +
                    " SELECT studentid" +
                    " FROM student_registration" +
                    " WHERE yos = 2);";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lid);
            rs = prepStmt.executeQuery();

            while (rs.next()) {
                StudentReport studentReport = new StudentReport(rs.getInt("studentid"));
                studentReport.generateReport();
            }

            printReport("\nYEAR 3", "\n");

            query = "SELECT studentid" +
                    " FROM tutor" +
                    " WHERE lecturerid = ?" +
                    " AND studentid IN (" +
                    " SELECT studentid" +
                    " FROM student_registration" +
                    " WHERE yos = 3);";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lid);
            rs = prepStmt.executeQuery();

            while (rs.next()) {
                StudentReport studentReport = new StudentReport(rs.getInt("studentid"));
                studentReport.generateReport();
            }

            printReport("\nYEAR 4", "\n");

            query = "SELECT studentid" +
                    " FROM tutor" +
                    " WHERE lecturerid = ?" +
                    " AND studentid IN (" +
                    " SELECT studentid" +
                    " FROM student_registration" +
                    " WHERE yos = 4);";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lid);
            rs = prepStmt.executeQuery();

            while (rs.next()) {
                StudentReport studentReport = new StudentReport(rs.getInt("studentid"));
                studentReport.generateReport();
            }

            printReport("\nYEAR 5", "\n");

            query = "SELECT studentid" +
                    " FROM tutor" +
                    " WHERE lecturerid = ?" +
                    " AND studentid IN (" +
                    " SELECT studentid" +
                    " FROM student_registration" +
                    " WHERE yos = 5);";
            prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lid);
            rs = prepStmt.executeQuery();

            while (rs.next()) {
                StudentReport studentReport = new StudentReport(rs.getInt("studentid"));
                studentReport.generateReport();
            }

            if (rs != null) {
                log.log(Level.INFO, "Fetched data successfully");
            } else {
                log.log(Level.SEVERE, "No student exists with this id");
            }

        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }

    private void printReport(String label, String line) {

        System.out.print(label + ": ");
        System.out.println(line);
    }
}
