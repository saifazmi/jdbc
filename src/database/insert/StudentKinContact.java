package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class StudentKinContact {

    private static final Logger log = Logger.getLogger(StudentKinContact.class.getName());

    private int studentID;
    private String name;
    private String email;
    private String postalAddress;

    private final String query = "INSERT INTO " +
            " Student_Kin_Contact (studentid, name, email, postaladdress)" +
            " VALUES (?,?,?,?)";

    public StudentKinContact(int studentID, String name, String email, String postalAddress) {
        this.studentID = studentID;
        this.name = name.substring(0, 1).toUpperCase() + name.substring(1);
        this.email = email.toLowerCase();
        this.postalAddress = postalAddress;
    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, studentID);
            prepStmt.setString(2, name);
            prepStmt.setString(3, email);
            prepStmt.setString(4, postalAddress);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
