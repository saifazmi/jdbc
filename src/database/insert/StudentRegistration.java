package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class StudentRegistration {

    private static final Logger log = Logger.getLogger(StudentRegistration.class.getName());

    private int studentID;
    private int yos;
    private String regID;

    private final String query = "INSERT INTO " +
            " Student_Registration (studentid, yos, regid)" +
            " VALUES (?,?,?)";

    public StudentRegistration(int studentID, int yos, String regID) {
        this.studentID = studentID;
        this.yos = yos;
        this.regID = regID.toUpperCase();
    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1,studentID);
            prepStmt.setInt(2, yos);
            prepStmt.setString(3, regID);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
