package database.insert;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class Student {

    private static final Logger log = Logger.getLogger(Student.class.getName());

    private int studentID;
    private String titleID;
    private String foreName;
    private String familyName;
    private Date dob;

    private final String query = "INSERT INTO " +
            " Student (studentid, titleid, forename, familyname, dob)" +
            " VALUES (?,?,?,?,?)";

    public Student(int studentID, String titleID, String foreName, String familyName, Date dob) {
        this.studentID = studentID;
        this.titleID = titleID.substring(0, 1).toUpperCase() + titleID.substring(1).toLowerCase();
        this.foreName = foreName.substring(0, 1).toUpperCase() + foreName.substring(1).toLowerCase();
        this.familyName = familyName.substring(0, 1).toUpperCase() + familyName.substring(1).toLowerCase();
        this.dob = dob;

    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, studentID);
            prepStmt.setString(2, titleID);
            prepStmt.setString(3, foreName);
            prepStmt.setString(4, familyName);
            prepStmt.setDate(5, dob);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            } else {
                log.log(Level.SEVERE, "Failed! to update " + numberOfRowsInserted + " rows.");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
