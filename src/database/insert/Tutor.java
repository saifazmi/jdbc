package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class Tutor {

    private static final Logger log = Logger.getLogger(Tutor.class.getName());

    private int studentID;
    private String lecturerID;

    private final String query = "INSERT INTO " +
            " Tutor (studentid, lecturerid)" +
            " VALUES (?,?)";

    public Tutor(int studentID, String lecturerID) {
        this.studentID = studentID;
        this.lecturerID = lecturerID;
    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, studentID);
            prepStmt.setString(2, lecturerID);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            } else {
                log.log(Level.SEVERE, "Failed! to insert " + numberOfRowsInserted + " row.");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }

    public int update() {

        int numberOfRowsUpdated = 0;
        final String updateQuery = "UPDATE Tutor " +
                " SET lecturerid = ?" +
                " WHERE studentid = ?;";

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(updateQuery);
            prepStmt.setString(1, lecturerID);
            prepStmt.setInt(2, studentID);
            numberOfRowsUpdated = prepStmt.executeUpdate();

            if (numberOfRowsUpdated > 0) {
                log.log(Level.INFO, numberOfRowsUpdated + " row updated!");
            } else {
                log.log(Level.SEVERE, "Failed! to update " + numberOfRowsUpdated + " rows.");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsUpdated;
    }
}
