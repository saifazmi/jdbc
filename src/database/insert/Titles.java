package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class Titles {

    private static final Logger log = Logger.getLogger(Titles.class.getName());

    private String titleID;
    private String description;

    private final String query = "INSERT INTO Titles (titleid, description)" +
            "VALUES (?,?);";

    public Titles(String titleID, String description) {

        this.titleID = titleID.substring(0, 1).toUpperCase() + titleID.substring(1).toLowerCase();
        this.description = description.toLowerCase();
    }

    public int insert() {

        int numberOfRowsInserted = 0;
        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, titleID);
            prepStmt.setString(2, description);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
