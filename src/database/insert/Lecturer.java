package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class Lecturer {

    private static final Logger log = Logger.getLogger(Lecturer.class.getName());

    private String lecturerID;
    private String titleID;
    private String foreName;
    private String familyName;

    private final String query = "INSERT INTO " +
            " Lecturer (lecturerid, titleid, forename, familyname)" +
            " VALUES (?,?,?,?)";

    public Lecturer(String lecturerID, String titleID, String foreName, String familyName) {
        this.lecturerID = lecturerID;
        this.titleID = titleID.substring(0, 1).toUpperCase() + titleID.substring(1).toLowerCase();
        this.foreName = foreName.substring(0, 1).toUpperCase() + foreName.substring(1).toLowerCase();
        this.familyName = familyName.substring(0, 1).toUpperCase() + familyName.substring(1).toLowerCase();
    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lecturerID);
            prepStmt.setString(2, titleID);
            prepStmt.setString(3, foreName);
            prepStmt.setString(4, familyName);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
