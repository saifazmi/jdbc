package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class StudentContact {

    private static final Logger log = Logger.getLogger(StudentContact.class.getName());

    private int studentID;
    private String email;
    private String postalAddress;

    private final String query = "INSERT INTO " +
            " Student_Contact (studentid, email, postaladdress)" +
            " VALUES (?,?,?)";

    public StudentContact(int studentID, String email, String postalAddress) {
        this.studentID = studentID;
        this.email = email.toLowerCase();
        this.postalAddress = postalAddress;
    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setInt(1, studentID);
            prepStmt.setString(2, email);
            prepStmt.setString(3, postalAddress);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
