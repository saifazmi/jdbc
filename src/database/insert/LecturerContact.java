package database.insert;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 29/10/15
 */
public class LecturerContact {

    private static final Logger log = Logger.getLogger(LecturerContact.class.getName());

    private String lecturerID;
    private String office;
    private String email;

    private final String query = "INSERT INTO " +
            " Lecturer_Contact (lecturerid, office, email)" +
            " VALUES (?,?,?)";

    public LecturerContact(String lecturerID, String office, String email) {
        this.lecturerID = lecturerID;
        this.office = office.toUpperCase();
        this.email = email.toLowerCase();
    }

    public int insert() {

        int numberOfRowsInserted = 0;

        try (Connection dbConn = getConnection()) {

            PreparedStatement prepStmt = dbConn.prepareStatement(query);
            prepStmt.setString(1, lecturerID);
            prepStmt.setString(2, office);
            prepStmt.setString(3, email);
            numberOfRowsInserted = prepStmt.executeUpdate();

            if (numberOfRowsInserted > 0) {
                log.log(Level.INFO, numberOfRowsInserted + " row inserted!");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return numberOfRowsInserted;
    }
}
