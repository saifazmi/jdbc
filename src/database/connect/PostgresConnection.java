package database.connect;

import database.properties.DBProperty;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author : saif
 * @project : UniDB
 * @date : 28/10/15
 */
public final class PostgresConnection {

    private static final Logger log = Logger.getLogger(PostgresConnection.class.getName());

    private PostgresConnection() {

    }

    public static Connection getConnection() {

        DBProperty dbProperty = new DBProperty();

        String drivers = dbProperty.getDrivers();

        String serverURL = dbProperty.getServerURL();
        String dbName = dbProperty.getDbName();

        String url = serverURL + dbName;
        String username = dbProperty.getUsername();
        String password = dbProperty.getPassword();

        System.setProperty("jdbc.drivers", drivers);

        Connection dbConn = null;

        try {
            dbConn = DriverManager.getConnection(url, username, password);

            if (dbConn != null) {
                log.log(Level.INFO, "Database accessed!");
            } else {
                log.log(Level.SEVERE, "Failed to make dbConnection");
            }
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }

        return dbConn;
    }
}
