package tests;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import static database.connect.PostgresConnection.getConnection;

/**
 * @author : saif
 * @project : UniDB
 * @date : 23/10/15
 */
public class TestDBConnection {

    private static final Logger log = Logger.getLogger(TestDBConnection.class.getName());

    public static void main(String[] args) {

        try (Connection conn = getConnection()) {
        } catch (SQLException e) {
            log.log(Level.SEVERE, e.toString(), e);
            e.printStackTrace();
        }
    }
}