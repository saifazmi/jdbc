/* Commands for debugging */

-- To check tables exists
SELECT table_name
FROM information_schema.tables
WHERE table_schema = 'ssc_ex'
ORDER BY table_name;

-- Describing tables
SELECT
  column_name,
  data_type,
  character_maximum_length
FROM INFORMATION_SCHEMA.COLUMNS
WHERE table_name = 'registration_type';

-- Adding new schema for easily managing db drops
DROP SCHEMA ssc_ex CASCADE;
CREATE SCHEMA ssc_ex;
SET SCHEMA 'ssc_ex';