SET SCHEMA 'ssc_ex';

CREATE TABLE Titles (
  titleID     CHARACTER(6) PRIMARY KEY,
  description TEXT NOT NULL UNIQUE,

  CONSTRAINT titlesList CHECK (titleID IN ('Mr', 'Ms', 'Miss', 'Mrs', 'Mx', 'Prof', 'Dr', 'Master'))
);

CREATE TABLE Registration_Type (
  regID       CHARACTER(2) PRIMARY KEY,
  description TEXT NOT NULL UNIQUE,

  CONSTRAINT regTypes CHECK (regID IN ('NR', 'RP', 'EX', 'TF'))
);

CREATE TABLE Student (
  studentID  INTEGER PRIMARY KEY,
  titleID    CHARACTER(6) NOT NULL,
  foreName   TEXT         NOT NULL,
  familyName TEXT         NOT NULL,
  dob        DATE,

  FOREIGN KEY (titleID) REFERENCES Titles (titleID)
  ON DELETE RESTRICT
);

CREATE TABLE Student_Registration (
  studentID INTEGER      NOT NULL UNIQUE,
  yos       INTEGER      NOT NULL,
  regID     CHARACTER(2) NOT NULL,

  FOREIGN KEY (studentID) REFERENCES Student (studentID)
  ON DELETE CASCADE,

  CONSTRAINT yearOfStudyLimit CHECK (yos BETWEEN 1 AND 5),

  FOREIGN KEY (regID) REFERENCES Registration_Type (regID)
  ON DELETE RESTRICT
);

CREATE TABLE Student_Contact (
  studentID     INTEGER NOT NULL UNIQUE,
  email         TEXT    NOT NULL UNIQUE,
  postalAddress TEXT,

  FOREIGN KEY (studentID) REFERENCES Student (studentID)
  ON DELETE CASCADE
);

CREATE TABLE Student_Kin_Contact (
  studentID     INTEGER NOT NULL UNIQUE,
  name          TEXT    NOT NULL,
  email         TEXT    NOT NULL UNIQUE,
  postalAddress TEXT,

  FOREIGN KEY (studentID) REFERENCES Student (studentID)
  ON DELETE CASCADE
);

CREATE TABLE Lecturer (
  lecturerID CHARACTER(7) PRIMARY KEY,
  titleID    CHARACTER(6) NOT NULL,
  foreName   TEXT         NOT NULL,
  familyName TEXT         NOT NULL,

  FOREIGN KEY (titleID) REFERENCES Titles (titleID)
  ON DELETE RESTRICT
);

CREATE TABLE Lecturer_Contact (
  lecturerID CHARACTER(7) NOT NULL,
  office     CHARACTER(4),
  email      TEXT         NOT NULL UNIQUE,

  FOREIGN KEY (lecturerID) REFERENCES Lecturer (lecturerID)
  ON DELETE CASCADE
);

CREATE TABLE Tutor (
  studentID  INTEGER      NOT NULL UNIQUE,
  lecturerID CHARACTER(7) NOT NULL,

  FOREIGN KEY (studentID) REFERENCES Student (studentID)
  ON DELETE CASCADE,

  FOREIGN KEY (lecturerID) REFERENCES Lecturer (lecturerID)
  ON DELETE RESTRICT
);