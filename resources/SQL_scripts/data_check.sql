SET SCHEMA 'ssc_ex';

SELECT *
FROM Titles
ORDER BY titleid;

SELECT *
FROM Registration_Type
ORDER BY regid;

SELECT *
FROM lecturer
ORDER BY forename;

SELECT *
FROM lecturer_contact
ORDER BY lecturerid;

SELECT *
FROM student
ORDER BY studentid;

SELECT *
FROM student_contact;

SELECT *
FROM tutor
ORDER BY studentid;

SELECT *
FROM student_kin_contact
ORDER BY studentid;

SELECT *
FROM student_registration
ORDER BY studentid;

DELETE FROM tutor;
DELETE FROM student_contact;
DELETE FROM student;
DELETE FROM lecturer_contact;
DELETE FROM lecturer;
DELETE FROM Registration_Type;
DELETE FROM Titles;
DELETE FROM student_kin_contact;
DELETE FROM student_registration;